package br.com.gusta.themeals.info;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class InfoController {

    @RequestMapping("/info")
    public String get() {
        return "/info/index.html";
    }

}
