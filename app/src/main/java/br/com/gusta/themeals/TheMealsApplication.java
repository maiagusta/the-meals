package br.com.gusta.themeals;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheMealsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TheMealsApplication.class, args);
	}

}
