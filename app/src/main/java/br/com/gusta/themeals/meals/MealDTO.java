package br.com.gusta.themeals.meals;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class MealDTO {

    public List<Meal> meals;

    public List<Meal> getMeals() {
        return meals;
    }

    @Override
    public String toString() {
        return "MealDTO{" + "meals='" + meals + '\'' + '}';
    }
}
