package br.com.gusta.themeals.meals;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class MealRepository {

    public List<Meal> getAll() {
        
        RestTemplate restTemplate = new RestTemplate();
        List<Meal> list = restTemplate
                .getForObject("https://www.themealdb.com/api/json/v1/1/search.php?s=", MealDTO.class).getMeals();

        return list;
    }
}
