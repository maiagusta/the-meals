package br.com.gusta.themeals.meals;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MealsController {

    @Autowired
    private MealRepository mealRepository;

    @RequestMapping("/meals")
    public List<Meal> get() {
        return mealRepository.getAll();
    }

}
