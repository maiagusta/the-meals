angular.module('TheMealsApp').controller('IndexController', ['$scope', '$http', '$window', '$location', function ($scope, $http, $window) {
    const urlMeals = `${$window.location.href}/meals`;
    $scope.mealsFiltered = [];

    $scope.selectMeal = function (meal) {
        var landingUrl = $window.location.href + "info/?id=" + meal.idMeal;
        $window.location.href = landingUrl;
    };

    const getMeals = async function () {
        if (localStorage.getItem('MEALS')) {
            $scope.meals = JSON.parse(localStorage.getItem('MEALS'));
            $scope.mealsFiltered = $scope.meals;

        } else {
            await $http.get(urlMeals)
                .then((response) => {
                    $scope.meals = response.data;
                    $scope.mealsFiltered = $scope.meals;
                    saveMealsToLocalStorage();
                }, function () {
                    $scope.mealsFiltered = $scope.meals;
                    $scope.meals = [];
                });
        }
    };

    const saveMealsToLocalStorage = function () {
        localStorage.clear();
        localStorage.setItem('MEALS', JSON.stringify($scope.meals));
    };

    $scope.onChangeFilter = function () {
        $scope.mealsFiltered = [];

        if (!$scope.filter) {
            $scope.mealsFiltered = $scope.meals;
        } else {
            var filters = $scope.filter.toString().split(" ");

            filters.forEach(filter => {
                $scope.meals.forEach(meal => {
                    if (meal.strMeal.toLowerCase().includes(filter.toLowerCase()) ||
                        meal.strArea.toLowerCase().includes(filter.toLowerCase()) ||
                        meal.strCategory.toLowerCase().includes(filter.toLowerCase())) {

                        $scope.mealsFiltered.push(meal);
                    }
                });

            });
        }
    };


    getMeals();
}]);

