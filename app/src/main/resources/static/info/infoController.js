angular.module('info').controller('InfoController', ['$scope', '$window', function ($scope, $window) {
    const idMeal = $window.location.search.substr(1).toString().split("=")[1];
    $scope.meals = JSON.parse(localStorage.getItem('MEALS'));

    $scope.meal = $scope.meals.find(function (m) {
        return m.idMeal.toString() === idMeal.toString();
    });

    $scope.backPage = function () {
        var paths = $window.location.pathname.replaceAll("/", " ").trim().split(" ");
        var landingUrl = "http://" + $window.location.host + "/";
        if (paths.length > 1) {
            landingUrl = landingUrl + paths[0];
        }

        $window.location.href = landingUrl;
    };

    $scope.ingredients = [{ description: $scope.meal.strIngredient1, measure: $scope.meal.strMeasure1 },
    { description: $scope.meal.strIngredient2, measure: $scope.meal.strMeasure2 },
    { description: $scope.meal.strIngredient3, measure: $scope.meal.strMeasure3 },
    { description: $scope.meal.strIngredient4, measure: $scope.meal.strMeasure4 },
    { description: $scope.meal.strIngredient5, measure: $scope.meal.strMeasure5 },
    { description: $scope.meal.strIngredient6, measure: $scope.meal.strMeasure6 },
    { description: $scope.meal.strIngredient7, measure: $scope.meal.strMeasure7 },
    { description: $scope.meal.strIngredient8, measure: $scope.meal.strMeasure8 },
    { description: $scope.meal.strIngredient9, measure: $scope.meal.strMeasure9 },
    { description: $scope.meal.strIngredient10, measure: $scope.meal.strMeasure10 },
    { description: $scope.meal.strIngredient11, measure: $scope.meal.strMeasure11 },
    { description: $scope.meal.strIngredient12, measure: $scope.meal.strMeasure12 },
    { description: $scope.meal.strIngredient13, measure: $scope.meal.strMeasure13 },
    { description: $scope.meal.strIngredient14, measure: $scope.meal.strMeasure14 },
    { description: $scope.meal.strIngredient15, measure: $scope.meal.strMeasure15 },
    { description: $scope.meal.strIngredient16, measure: $scope.meal.strMeasure16 },
    { description: $scope.meal.strIngredient17, measure: $scope.meal.strMeasure17 },
    { description: $scope.meal.strIngredient18, measure: $scope.meal.strMeasure18 },
    { description: $scope.meal.strIngredient19, measure: $scope.meal.strMeasure19 },
    { description: $scope.meal.strIngredient20, measure: $scope.meal.strMeasure20 },
    ];

}]);