package br.com.gusta.themeals;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import br.com.gusta.themeals.meals.MealRepository;

@SpringBootTest
class MealsControllerTests {

    @Autowired
    private MealRepository respository;

    @Test
    void testingIfaListIsNotNull() {
        assertTrue(respository.getAll() != null && !respository.getAll().isEmpty());
    }

}
