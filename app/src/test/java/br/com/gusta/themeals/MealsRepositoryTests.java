package br.com.gusta.themeals;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MealsRepositoryTests {

    @Test
    void testingIfaConectionIsNotNull() throws IOException {
        URL url = new URL("https://www.themealdb.com/api/json/v1/1/search.php?s=");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        assertTrue(con.getResponseCode() == 200);
        con.disconnect();
    }

}
